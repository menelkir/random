#!/usr/bin/env bash
# By Notlet 
set -e

if [[ -z $1 || $1 -lt 2 ]]; then
	echo 'Invalid amount entered (must be ≥2)'
	exit 1
fi

if [[ $(ls /dev/mapper/ | grep 'infiniteluks' ) ]]; then
	echo 'Cleaning up.'
	sudo umount /mnt
	for p in $(ls -r --sort version /dev/mapper/infiniteluks*); do
		echo "Closing $p"
		sudo cryptsetup close $p
	done
fi

echo 'Preparing file.'
if [[ -e ./infiniteluks ]]; then rm infiniteluks; fi
fallocate -l 10G infiniteluks

echo "Doing pass 1."
sudo cryptsetup luksFormat infiniteluks -d ./passphrase.txt -q -v --type luks1
sudo cryptsetup open infiniteluks infiniteluks1 -d ./passphrase.txt -q -v

for i in $(seq 2 $1); do
	echo "Doing pass $i/$1."
	sudo cryptsetup luksFormat /dev/mapper/infiniteluks$(($i-1)) -d ./passphrase.txt -q -v --type luks1
	sudo cryptsetup open /dev/mapper/infiniteluks$(($i-1)) infiniteluks$i -d ./passphrase.txt -q -v
done

echo 'Formatting as Ext4 and mounting to /mnt'
sudo mkfs.ext4 -v /dev/mapper/infiniteluks$1
sudo mount /dev/mapper/infiniteluks$1 /mnt

echo 'Enjoy your madness.'
